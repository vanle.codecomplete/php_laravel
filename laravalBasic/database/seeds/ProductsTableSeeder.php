<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product')->insert([
            ['name'=>'Bánh tráng cuốn thịt heo','image'=>'','price' => 234, 'quantity'=>23, 'description'=>'Ngon hết sẩy'],
            ['name'=>'Bánh xèo tôm','image'=>'','price' => 240, 'quantity'=>23, 'description'=>'Ngon quá chừng'],
            ['name'=>'Ram cuốn cải','image'=>'','price' => 340, 'quantity'=>23, 'description'=>'Ngon khỏi bàn'],
        ]);
    }
}
