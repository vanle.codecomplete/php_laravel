
<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Title Page</title>

        <!-- Bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.3/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="container" style="width: 500px;" >
            <form action="{{ route('product/post_add') }}" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
            @csrf
                    <div class="form-group">
                        <legend>Add product </legend>
                    </div>
                    <div class="row" style="padding:8px 16px" >
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="">Name</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <input type="text" name="name" id="name" class="form-control" value="" required="required">
                        </div>
                    </div>
                    <div class="row" style="padding:8px 16px">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="">Image</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <input type="text" name="img" id="img" class="form-control" value="" required="required">
                        </div>
                    </div>
                    <div class="row" style="padding:8px 16px">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="">Price</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <input type="text" name="price" id="price" class="form-control" value="" required="required">
                        </div>
                    </div>
                    <div class="row" style="padding:8px 16px">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="">Quanity</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <input type="text" name="quantity" id="quantity" class="form-control" value="" required="required">
                        </div>
                    </div>
                    <div class="row" style="padding:8px 16px">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <label for="">Description</label>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            <textarea rows="7" cols="70" required="required" name="description"  placeholder="Mô tả tour" style="width: 100%">

                            </textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">Add</button>
                            <a href="{{ route('product') }}" class="btn btn-small btn-danger iframe">Back to Home</a>
                        </div>
                    </div>
            </form>
        </div>
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
