<?php
use Illuminate\Support\Facades\Route;
?>
<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Title Page</title>

        <!-- Bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.3/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
        <legend>Products</legend>
        <a href="{{ route('product/get_add') }}" class="btn btn-small btn-info iframe">Click to add product</a>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach($products as $product){
                ?>
                <tr>
                    <td><?php echo $product->id?></td>
                    <td><?php echo $product->name?></td>
                    <td><?php echo $product->image?></td>
                    <td><?php echo $product->price?></td>
                    <td><?php echo $product->quantity?></td>
                    <td><?php echo $product->description?></td>
                    <td>
                    <a href="{{route('product/delete', $product->id)}}">Delete</a>
                    <a href="{{route('product/get_edit', $product->id)}}" >Edit</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        </div>
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
