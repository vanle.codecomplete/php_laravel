<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/product', function () {
//     return view('product.viewProduct');
// });

Route::get('product',[
    'as' => 'product',
    'uses' => 'productController@view'
] );

Route::get('editProduct/{id}',[
    'as' => 'product/get_edit',
    'uses' => 'productController@getEdit'
] );

Route::post('editProduct/{id}',[
    'as' => 'product/post_edit',
    'uses' => 'productController@postEdit'
] );

Route::post('addProduct',[
    'as' => 'product/post_add',
    'uses' => 'productController@postAdd'
] );

Route::get('addProduct',[
    'as' => 'product/get_add',
    'uses' => 'productController@getAdd'
] );

Route::get('delProduct/{id}',[
    'as' => 'product/delete',
    'uses' => 'productController@delete'
] );
