<?php

namespace App\Http\Middleware;

use Closure;

class CheckPrice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if($request->price)
        return $next($request);
    }
}
