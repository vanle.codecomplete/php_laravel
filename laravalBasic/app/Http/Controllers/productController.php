<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\product;
class productController extends Controller
{
   public function view(){
       $products = product::get();
       return view('product.viewProduct')->with('products',$products);
   }

   public function getEdit($id){
     $product_edit = product::find($id);
     return view('product.editProduct')->with('product_edit', $product_edit);
   }

   public function postEdit(Request $request, $id){
       $product_edit =product::find($id);
       $product_edit->name= $request->input('name');
       $product_edit->image= $request->input('img');
       $product_edit->price= $request->input('price');
       $product_edit->quantity= $request->input('quantity');
       $product_edit->description= $request->description;
       $product_edit->save();
       return view('product.editProduct')->with('product_edit', $product_edit);
   }

   public function delete($id){
    $product_del =product::find($id);
    $product_del->delete($id);
    return back()->with('alert','Deleted');
   }

   public function postAdd(Request $request){
       $product_add = new product();
       $product_add->name= $request->input('name');
       $product_add->image= $request->input('img');
       $product_add->price= $request->input('price');
       $product_add->quantity= $request->input('quantity');
       $product_add->description= $request->description;
       $product_add->save();
       return back();
   }

   public function getAdd(){
     return view('product.addProduct');
  }
}
?>
